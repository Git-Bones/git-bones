# Git-Bones

Git-Bones is a simple application that allows users to quickly copy git project templates into a new project across different environments. The intent of the application is to enable consistent git project structures, templates, and CI models.